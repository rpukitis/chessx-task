import React from 'react'
import { MemoryRouter } from 'react-router-dom'
import { render as rtlRender } from '@testing-library/react'
import ConfigProvider from '../packages/chessx-client/src/components/providers/ConfigProvider'

export function render(ui, options = {}) {
  const initialEntries = [options.route || '/']

  return rtlRender(
    <ConfigProvider>
      <MemoryRouter initialEntries={initialEntries}>{ui}</MemoryRouter>
    </ConfigProvider>,
    options,
  )
}

export * from '@testing-library/react'
