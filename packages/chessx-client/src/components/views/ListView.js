import React from 'react'
import styled, { css } from 'styled-components'
import { useQuery } from 'react-query'
import { Link } from 'react-router-dom'
import { useConfig } from '../providers/ConfigProvider'
import BaseCard from '../elements/Card'
import Loading from '../elements/Loading'
import Error from '../elements/Error'

const List = styled.ul`
  margin: -16px 0;
  padding: 0;

  li {
    margin: 16px 0;
  }
`

const Card = styled(BaseCard)`
  cursor: pointer;

  &:hover {
    background-color: #edf2f7;
  }
`

const Pagination = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 32px;

  & button:not(:last-child) {
    margin-right: 8px;
  }
`

const PaginationButton = styled.button`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 40px;
  height: 40px;
  color: #2d3748;
  background-color: #e2e8f0;
  border-radius: 50%;
  user-select: none;
  cursor: pointer;

  ${props =>
    props.selected &&
    css`
      && {
        background-color: #cbd5e0;
      }
    `}
`

function fetchItems(_, { url, page = 1, limit = 10 }) {
  return fetch(`${url}/items?page=${page}&limit=${limit}`, {
    method: 'GET',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  }).then(res => {
    if (!res.ok) {
      throw new Error(res.statusText)
    }
    return res.json()
  })
}

export default function ListView() {
  const { url } = useConfig()
  const [page, setPage] = React.useState(1)
  const { status, data, error } = useQuery(['items', { url, page }], fetchItems)

  if (status === 'loading') {
    return <Loading />
  }

  if (status === 'error') {
    return <Error>Error: {error.message}</Error>
  }

  const items = data.map(item => ({
    ...item,
    text: item.text.length > 300 ? `${item.text.slice(0, 300)}...` : item.text,
  }))

  return (
    <>
      <List>
        {items.map(item => (
          <li key={item.id}>
            <Link to={`/item/${item.id}`}>
              <Card>
                <h2>{item.title}</h2>
                <p>{item.text}</p>
              </Card>
            </Link>
          </li>
        ))}
      </List>
      <Pagination>
        {[1, 2].map((value, index) => (
          <PaginationButton
            key={index}
            selected={page === index + 1}
            onClick={() => setPage(value)}
          >
            {value}
          </PaginationButton>
        ))}
      </Pagination>
    </>
  )
}
