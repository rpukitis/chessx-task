import React from 'react'
import styled from 'styled-components'
import { useQuery } from 'react-query'
import { useParams, useNavigate } from 'react-router-dom'
import Card from '../elements/Card'
import BackIcon from '../elements/BackIcon'
import Loading from '../elements/Loading'
import Error from '../elements/Error'
import { useConfig } from '../providers/ConfigProvider'

const BackButton = styled.button`
  display: flex;
  align-items: center;
  margin: 16px 0;
  font-weight: 700;
  font-size: 0.875rem;
  line-height: 1;
  color: #4a5568;
  cursor: pointer;
`

function fetchItem(_, { url, id }) {
  return fetch(`${url}/item/${id}`, {
    method: 'GET',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  }).then(res => {
    if (!res.ok) {
      throw new Error(res.statusText)
    }
    return res.json()
  })
}

export default function ItemView() {
  const navigate = useNavigate()
  const { url } = useConfig()
  const { id } = useParams()
  const { status, data, error } = useQuery(['item', { url, id }], fetchItem)

  if (status === 'loading') {
    return <Loading />
  }

  if (status === 'error') {
    return <Error>Error: {error.message}</Error>
  }

  return (
    <>
      <BackButton onClick={() => navigate(-1)}>
        <BackIcon />
        Go back to list view
      </BackButton>
      <Card>
        <h2>{data.title}</h2>
        <p>{data.text}</p>
      </Card>
    </>
  )
}
