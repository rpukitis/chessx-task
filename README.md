# chessx task

## What it is?

This is chessx task

## How to use

This project is monorepo, therefore you must have yarn v1 installed on your system. Optionally, you must have installed postgresql, otherwise please refer to [knexjs documentation](http://knexjs.org/) and update knexfile.js to your db of choice.

Update .env file located at packages/chessx-server and seed db

```bash
yarn
yarn migrate
yarn seed
```

Install it and run in development mode:

```bash
yarn
yarn start
```

1. server runs at http://localhost:3000/
1. client runs at http://localhost:8000/

Install it and build:

```bash
yarn
yarn build
```

Run integration tests:

```bash
yarn test
```

Format code:

```bash
yarn format
```