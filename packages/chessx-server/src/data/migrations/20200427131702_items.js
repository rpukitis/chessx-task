exports.up = knex =>
  knex.schema.createTable('items', table => {
    table.increments()
    table.string('title').notNullable()
    table.text('text').notNullable()
  })

exports.down = knex => knex.schema.dropTableIfExists('items')
