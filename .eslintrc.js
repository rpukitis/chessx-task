module.exports = {
  env: {
    browser: true,
    es6: true,
    'jest/globals': true,
  },
  parser: 'babel-eslint',
  extends: ['standard', 'standard-react', 'prettier'],
  plugins: ['jest', 'react', 'react-hooks'],
  rules: {
    strict: 0,
    'jsx-quotes': ['error', 'prefer-double'],
    'comma-dangle': ['error', 'always-multiline'],
    'no-console': 'error',
    'no-multiple-empty-lines': 'error',
    'prefer-template': 'error',
    'prefer-const': 'error',
    'no-undef': 0,
    'react/prop-types': 0,
    'no-async-promise-executor': 0,
    'react-hooks/rules-of-hooks': 'error',
    'react-hooks/exhaustive-deps': 'warn',
  },
}
