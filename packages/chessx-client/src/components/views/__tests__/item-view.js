import React from 'react'
import faker from 'faker'
import { render, waitForElementToBeRemoved } from 'app-test-utils'
import ItemView from '../ItemView'

faker.seed(101)

const item = {
  id: 1,
  title: faker.lorem.words(1),
  text: faker.lorem.paragraph(1),
}

describe('Item View', () => {
  beforeEach(() => {
    fetch.resetMocks()
  })

  it('should render item view', async () => {
    fetch.mockResponse(JSON.stringify(item))
    const { queryByText, getByText } = render(<ItemView />, {
      route: '/',
    })

    await waitForElementToBeRemoved(() => queryByText('loading...'))
    expect(fetch.mock.calls.length).toEqual(1)

    getByText(item.title)
    getByText(item.text)
  })
})
