import React from 'react'
import ErrorIcon from './ErrorIcon'

export default function Error({ children }) {
  return (
    <div
      css={`
        display: flex;
        align-items: center;
        font-weight: 500;
        font-size: 1rem;

        svg {
          margin-right: 8px;
        }
      `}
    >
      <ErrorIcon fill="#DD423E" />
      {children}
    </div>
  )
}
