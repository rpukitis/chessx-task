import styled from 'styled-components'

const Card = styled.div`
  padding: 16px;
  font-family: inherit;
  color: #4a5568;
  border-radius: 4px;
  box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 1px 2px 0 rgba(0, 0, 0, 0.06);

  h2 {
    margin: 0;
    font-weight: 700;
    font-size: 1.25rem;
    line-height: 1;
  }

  p {
    margin: 16px 0 0 0;
    font-size: 1rem;
    line-height: 1.25;
  }
`

export default Card
