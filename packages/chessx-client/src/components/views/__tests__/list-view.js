import React from 'react'
import faker from 'faker'
import { render, waitForElementToBeRemoved, fireEvent } from 'app-test-utils'
import ListView from '../ListView'

faker.seed(101)

const items = Array.from({ length: 20 }, (_, index) => ({
  id: index + 1,
  title: faker.lorem.words(1),
  text: faker.lorem.paragraph(1),
}))

describe('List View', () => {
  beforeEach(() => {
    fetch.resetMocks()
  })

  it('should render list view', async () => {
    const page1 = items.slice(0, 10)
    const page2 = items.slice(9, 20)

    fetch.mockResponse(JSON.stringify(page1))
    const { queryByText, getAllByText, getByText } = render(<ListView />, {
      route: '/',
    })

    await waitForElementToBeRemoved(() => queryByText('loading...'))
    expect(fetch.mock.calls.length).toEqual(1)

    page1.forEach(item => {
      getAllByText(item.title)
      getAllByText(item.text)
    })

    fetch.mockResponse(JSON.stringify(page2))
    fireEvent.click(getByText('2'))
    await waitForElementToBeRemoved(() => queryByText('loading...'))
    expect(fetch.mock.calls.length).toEqual(2)

    page2.forEach(item => {
      getAllByText(item.title)
      getAllByText(item.text)
    })
  })
})
