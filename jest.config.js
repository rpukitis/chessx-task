const path = require('path')

module.exports = {
  testEnvironment: 'jest-environment-jsdom',
  automock: false,
  moduleDirectories: ['node_modules', path.join(__dirname, 'test')],
  setupFiles: ['./test/setup.js'],
  setupFilesAfterEnv: [require.resolve('./test/script.js')],
  collectCoverageFrom: [
    '**/src/**/*.{js,jsx,ts,tsx}',
    '!src/index.tsx',
    '!src/App.tsx',
  ],
  watchPlugins: [
    'jest-watch-typeahead/filename',
    'jest-watch-typeahead/testname',
  ],
}
