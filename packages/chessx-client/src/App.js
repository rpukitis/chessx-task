import React from 'react'
import styled from 'styled-components'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import ConfigProvider from './components/providers/ConfigProvider'
import ListView from './components/views/ListView'
import ItemView from './components/views/ItemView'
import GlobalStyles from './components/elements/GlobalStyles'

const Container = styled.main`
  max-width: 680px;
  margin: 32px auto;

  @media (max-width: 680px) {
    margin: 32px 16px;
  }
`

export default function App() {
  return (
    <ConfigProvider>
      <Container>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<ListView />} />
            <Route path="/item/:id" element={<ItemView />} />
          </Routes>
        </BrowserRouter>
        <GlobalStyles />
      </Container>
    </ConfigProvider>
  )
}
