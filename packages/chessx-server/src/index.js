import express from 'express'
import cors from 'cors'
import db from './data/db'

const app = express()
app.use(cors())
app.use(express.json())

app.get('/', (req, res) => res.send(`hey, ho, let's go!`))

app.get('/items', async (req, res) => {
  const page = req.query.page || 1
  const limit = req.query.limit || 20
  const items = await db('items')
    .limit(limit)
    .offset((page - 1) * limit)
  res.json(items)
})

app.get('/item/:id', async (req, res) => {
  const [item] = await db('items').where('id', req.params.id)
  res.json(item)
})

app.listen({ port: 3000 }, () => {
  // eslint-disable-next-line no-console
  console.log('Example app listening at http://localhost:3000')
})
