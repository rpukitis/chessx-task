const faker = require('faker')

faker.seed(101)

const items = Array.from({ length: 20 }, (_, index) => ({
  id: index + 1,
  title: faker.lorem.words(3),
  text: faker.lorem.paragraph(10),
}))

exports.seed = knex =>
  knex('items')
    .del()
    .then(() => knex('items').insert(items))
